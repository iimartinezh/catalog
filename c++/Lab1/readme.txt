Bessel function Program

Execute "make" in the linux terminal,
then ./bessel value_of_n value_of_x value_of_e.
The code is for n being a integer number, and x is different of 0.


The code detects when "n" is not a integer, when "e" is not within 0<|e|<1, also when the sum's result in a 
NaN or Inf value gives you the aswer before that happened.
