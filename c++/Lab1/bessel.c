#include <math.h>
#include <stdio.h>
#include <string.h>
#include <stdlib.h>
//This program solves the bessel equation of the first kind. Where inputs are n,x and e 
int main (int argc, char *argv[]){
       	double x=atof(argv[2]), e=atof(argv[3]), n_doub=atof(argv[1]);
	int n= (int) n_doub, m, mn, fcount, factorialM, factorialMN, fmncount;
	double second, alpha1, alpha, M, alphanan;
// Robust process

if (e<0){
e=fabs(e);
printf("The tolerance its handle as an absolute value (e>0), therefore e=%e \n\n",e);
}

if(e>1) {
printf("Please enter a value of 'e' in 0<e<1, for better results of this type of equation \n\n");
return 0;
}

if(n != n_doub){ printf("*This bessel function only execute for integer of n, so only the integer part will be considered. \n\n\n");}


//this section of the code is for the first iteration when m is zero
m=0;
factorialM=1; //the first factorial 
mn=m+n;
factorialMN=1;
	for (fmncount=mn; fmncount>0; fmncount--) {
   	      factorialMN=factorialMN*fmncount;}//first factorial of sum m+n
 second=pow(x/2,(2*m)+n);
 M =(pow(-1,m)/(factorialM*factorialMN))*second;
		if (fabs(M)<e){ 
		alpha=(pow(-1,m)/(factorialM*factorialMN))*second;
		printf("Terms:%d   Alpha=%f \n\n", m, alpha);
		return 0;
		}
// This section is when m is more than zero
 while (fabs(M) > e) {
	alphanan=alpha; 
	alpha1=(pow(-1,m)/(factorialM*factorialMN))*second;
	alpha=alpha+alpha1; //sumatoria
	      
       	if(isnan(alpha) || isinf(alpha) ) { // parte por si el resultado es NaN
		printf("For value n=%d, x=%f, e=%e \n",n, x, e);
		printf("For that values the result converges as a  NaN or Inf, and the last value before this was: \n");
                printf("Terms:%d   Alpha (sum until M-1)=%e \n\n", m-1, alphanan);
	        return 0; 
		}
	factorialM=1; //return el valor de factorialM para reset
	m=m+1; //exponente y contador
	second=pow(x/2,(2*m)+n);
	
	for (fcount=m; fcount>0; fcount-- ) {
		factorialM=factorialM*fcount; } //factorial de M
	
	mn=m+n;//empieza factoral de m+n 
	factorialMN=1;
	for (fmncount=mn; fmncount>0; fmncount--) {
		factorialMN=factorialMN*fmncount;}  //Factorial de M+n
	
	M=((pow(-1,m)/(factorialM*factorialMN))*second);
 				
	} //determinar M para saber si seguir iterando
result:
printf("For values n=%d, x=%f, e=%e \n",n, x, e);
printf("Terms:%d   Alpha (sum until M-1)=%e \n\n (the value of  M<e is %e) \n\n", m-1, alpha, M);
return 0;
	}


