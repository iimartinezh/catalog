function dydt=lv(t,y)
 p=[0.0026 0.5058];
  dydt = [-p(1)*y(1)*y(2); p(1)*y(1)*y(2) - p(2)*y(2)];
  
end
