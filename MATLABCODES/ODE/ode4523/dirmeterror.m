function error = dirmeterror(p)
  
   days = [0 3 4 5 6 7 8 9 10 11 12 13 14];
  S = [762 740 650 400 250 120 80 50 20 18 15 13 10];
  I = [1 20 80 220 300 260 240 190 120 80 20 5 2];

    [t,y] = ode23(@directmetheq, days, [S(1); I(1)],[],p);
  
    dydtNew = (S'-y(:,1)).^2 + (I'-y(:,2)).^2 ;%%error least squares
  
    error = sum(dydtNew);
    
  
end