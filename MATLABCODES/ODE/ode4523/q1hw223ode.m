days = [0 3 4 5 6 7 8 9 10 11 12 13 14];
S = [762 740 650 400 250 120 80 50 20 18 15 13 10];
I = [1 20 80 220 300 260 240 190 120 80 20 5 2];

for i = 1:8
    
  Y2(i) = (1/I(i+2))*(I(i+3)-I(i+1))/2;
  Y1(i) = S(i+2);
end

%plot(Y1, Y2, 'o')

p = polyfit(Y1, Y2, 1)

f = polyval(p,Y1);

plot(Y1, Y2, 'o', Y1, f, '-')
legend('data', 'line fit')
pause


[t,y]=ode23(@lv, [0 14], [762 1], []);

subplot(2,1,1);
plot(t, y(:,1), days, S, 'o')
title('Susceptible population,model and data','FontSize',11)


subplot(2,1,2);
plot(t, y(:,2), days, I, 'o')
title('Infected population, model and data','FontSize',11)


[t,y]=ode45(@lv, days, [762 1], []);
error=sqrt(sum((S'-y(:,1)).^2+(I'-y(:,2)).^2))

