%y()--> x=1, y=2, z=3
f=@(t,y)[y(1)-y(1)*y(2); -y(2)+y(1)*y(2)-y(2)*y(3); -y(3)+y(2)*y(3)];
y=[0.5;1;2]; %initial conditions
t=0;
for i=0:300
plot(t,y(1),'b.',t,y(2),'r.',t,y(3),'g.'); hold on 
s=f(t,y)
y=y+0.0125*s; %euler
t=t+0.0125;
end
hold off
legend('x','y','z');